# frozen_string_literal: true

class ResultStatsAggregator
  def self.call
    new(Date.current).execute
  end

  def initialize(date)
    @target_date = date
  end

  def execute
    fetch_daily_stats_per_subject
    create_stats_record
  end

  private

  attr_reader :target_date, :stats

  def fetch_daily_stats_per_subject
    @stats ||=
      ResultsData
      .where(timestamp: target_date.all_day)
      .group(:subject)
      .select('subject, max(marks) as max_mark, min(marks) as min_mark, count(*) as total')
  end

  def create_stats_record
    stats.each do |stat|
      attributes = {
        subject: stat.subject,
        date: target_date,
        daily_low: stat.min_mark,
        daily_high: stat.max_mark,
        result_count: stat.total
      }
      DailyResultStat.create!(attributes)
    end
  end
end
