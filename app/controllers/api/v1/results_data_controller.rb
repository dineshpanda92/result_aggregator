# frozen_string_literal: true

class Api::V1::ResultsDataController < ApplicationController
  def create
    results_data = ResultsData.new(result_params)
    if results_data.save
      render :success
    else
      render json: { errors: results_data.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def result_params
    params.permit(:subject, :marks, :timestamp)
  end
end
