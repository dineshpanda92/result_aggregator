# frozen_string_literal: true

class ResultsData < ApplicationRecord
  validates :subject, presence: true
  validates :marks, presence: true
  validates :timestamp, presence: true
end
