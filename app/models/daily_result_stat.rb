# frozen_string_literal: true

class DailyResultStat < ApplicationRecord
  validates :subject, presence: true
  validates :date, presence: true
  validates :daily_low, presence: true
  validates :daily_high, presence: true
end
