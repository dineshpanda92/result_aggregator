## Run rspec specs

`bundle exec rspec spec`

## Run cucumber specs
`RAILS_ENV=test cucumber`

## Run whenever to update crontab which runs `ResultStatsAggregator` service everyday at 6 PM
`bundle exec whenever --update-crontab `
