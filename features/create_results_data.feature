Feature: Create Results Data

  Scenario: When I send valid params to /results_data endpoint
    When I send a post request to "api/v1/results_data" endpoint with valid params
    Then I should be able to create records in results_data table

  Scenario: When I send params with no subject
    When I send a post request to "api/v1/results_data" endpoint without specifying the "subject" in params
    Then I should receive an error message indicating "Subject can't be blank"

  Scenario: When I send params with no marks
    When I send a post request to "api/v1/results_data" endpoint without specifying the "marks" in params
    Then I should receive an error message indicating "Marks can't be blank"

  Scenario: When I send params with no timestamp
    When I send a post request to "api/v1/results_data" endpoint without specifying the "timestamp" in params
    Then I should receive an error message indicating "Timestamp can't be blank"
