# frozen_string_literal: true

When("I send a post request to {string} endpoint with valid params") do |endpoint|
  @response = post(endpoint, FactoryBot.attributes_for(:results_data, marks: 95))
end

Then("I should be able to create records in results_data table") do
  expect(@response.status).to eq(200)
  expect(ResultsData.count).to eq(1)
  expect(ResultsData.last.subject).to eq('Science')
  expect(ResultsData.last.marks).to eq(95)
end

When("I send a post request to {string} endpoint without specifying the {string} in params") do |endpoint, attr|
  @response = post(endpoint, FactoryBot.attributes_for(:results_data, marks: 95).except(attr.to_sym))
end

Then("I should receive an error message indicating {string}") do |error_message|
  expect(@response.status).to eq(422)
  expect(JSON.parse(@response.body)['errors']).to include(error_message)
end
