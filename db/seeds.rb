# frozen_string_literal: true

ResultsData.destroy_all

SAMPLE_SUBJECTS = %w[Science Math Physics].freeze
RESULT_CREATION_COUNT = 50

RESULT_CREATION_COUNT.times do
  FactoryBot.create(:results_data, subject: SAMPLE_SUBJECTS.sample)
end
