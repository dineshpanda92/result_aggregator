# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_09_10_075132) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "daily_result_stats", force: :cascade do |t|
    t.date "date", null: false
    t.string "subject", null: false
    t.float "daily_low"
    t.float "daily_high"
    t.integer "result_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "results_data", force: :cascade do |t|
    t.string "subject", null: false
    t.decimal "marks", null: false
    t.datetime "timestamp", precision: nil, null: false
    t.index ["subject"], name: "index_results_data_on_subject"
    t.index ["timestamp"], name: "index_results_data_on_timestamp"
  end

end
