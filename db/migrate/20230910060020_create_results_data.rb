class CreateResultsData < ActiveRecord::Migration[7.0]
  def change
    create_table :results_data do |t|
      t.string :subject, null: false, index: true
      t.decimal :marks, null: false
      t.timestamp :timestamp, null: false, index: true
    end
  end
end
