class CreateDailyResultStats < ActiveRecord::Migration[7.0]
  def change
    create_table :daily_result_stats do |t|
      t.date :date, null: false
      t.string :subject, null: false
      t.float :daily_low
      t.float :daily_high
      t.integer :result_count

      t.timestamps
    end
  end
end
