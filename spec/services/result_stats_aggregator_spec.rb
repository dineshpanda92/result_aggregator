# frozen_string_literal: true

require 'rails_helper'

def create_science_results
  FactoryBot.create(:results_data, marks: 10.50, timestamp: 10.hours.before)
  FactoryBot.create(:results_data, marks: 90.50, timestamp: 12.hours.before)
  FactoryBot.create(:results_data, marks: 80.75, timestamp: 10.hours.after)
  FactoryBot.create(:results_data, marks: 40.45, timestamp: 12.hours.after)
end

def create_physics_results
  subject = 'Physics'
  FactoryBot.create(:results_data, subject:, marks: 50.50, timestamp: 10.hours.before)
  FactoryBot.create(:results_data, subject:, marks: 51.50, timestamp: 12.hours.before)
  FactoryBot.create(:results_data, subject:, marks: 30.75, timestamp: 10.hours.after)
  FactoryBot.create(:results_data, subject:, marks: 20.75, timestamp: 10.hours.after)
  FactoryBot.create(:results_data, subject:, marks: 10.45, timestamp: 12.hours.after)
end

RSpec.describe ResultStatsAggregator do
  describe ".call" do
    context "when only one subject results are present" do
      it "creates result stats for current date" do
        Timecop.freeze(DateTime.current.beginning_of_day) do
          create_science_results
          expect { described_class.call }.to change { DailyResultStat.count }.by(1)
          record = DailyResultStat.last
          expect(record.daily_high).to eq(80.75)
          expect(record.daily_low).to eq(40.45)
          expect(record.subject).to eq("Science")
          expect(record.result_count).to eq(2)
          expect(record.date).to eq(Date.today)
        end
      end
    end

    context "when multiple subjects results are present" do
      it "creates result stats for current date" do
        Timecop.freeze(DateTime.current.beginning_of_day) do
          create_science_results
          create_physics_results

          expect { described_class.call }.to change { DailyResultStat.count }.by(2)
          rows = DailyResultStat.where(date: Date.current.all_day).order(:subject)
          physics_result_stats = rows.first
          science_result_stats = rows.last
          expect(science_result_stats.daily_high).to eq(80.75)
          expect(science_result_stats.daily_low).to eq(40.45)
          expect(science_result_stats.subject).to eq("Science")
          expect(science_result_stats.result_count).to eq(2)
          expect(physics_result_stats.daily_high).to eq(30.75)
          expect(physics_result_stats.daily_low).to eq(10.45)
          expect(physics_result_stats.subject).to eq("Physics")
          expect(physics_result_stats.result_count).to eq(3)
        end
      end
    end

    context "when there are no results data available for a day" do
      it "does not create any daily result stat" do
        Timecop.freeze(DateTime.current.beginning_of_day) do
          expect { described_class.call }.not_to change { DailyResultStat.count }
        end
      end
    end
  end
end
