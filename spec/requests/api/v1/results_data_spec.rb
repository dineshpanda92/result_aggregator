# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API::V1::ResultsDataController', type: :request do
  describe 'POST /results_data' do
    it 'returns http success with valid params' do
      expect do
        post api_v1_results_data_path,
             params: { subject: 'Science', timestamp: '2022-04-18 12:01:34.678', marks: 85.25 }
      end.to change { ResultsData.count }.by(1)
      expect(response).to have_http_status(:success)
    end

    context '422 error response' do
      it 'returns error when param is empty' do
        post api_v1_results_data_path
        expect(response).to have_http_status(:unprocessable_entity)
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['errors'])
          .to include("Subject can't be blank", "Marks can't be blank", "Timestamp can't be blank")
      end

      it 'returns error when marks param is missing' do
        post api_v1_results_data_path,
             params: { subject: 'Science', timestamp: '2022-04-18 12:01:34.678' }
        expect(response).to have_http_status(:unprocessable_entity)
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['errors']).to include("Marks can't be blank")
      end

      it 'returns error when subject param is missing' do
        post api_v1_results_data_path,
             params: { timestamp: '2022-04-18 12:01:34.678', marks: 80 }
        expect(response).to have_http_status(:unprocessable_entity)
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['errors']).to include("Subject can't be blank")
      end

      it 'returns error when timestamp param is missing' do
        post api_v1_results_data_path,
             params: { subject: 'Science', marks: 80 }
        expect(response).to have_http_status(:unprocessable_entity)
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['errors']).to include("Timestamp can't be blank")
      end
    end
  end
end
