# frozen_string_literal: true

FactoryBot.define do
  factory :results_data do
    subject { 'Science' }
    marks { Faker::Number.decimal(l_digits: 2, r_digits: 2) }
    timestamp { Faker::Time.between(from: 1.day.ago, to: DateTime.current) }
  end
end
